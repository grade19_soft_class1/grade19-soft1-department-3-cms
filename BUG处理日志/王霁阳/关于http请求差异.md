（1）GET

       当客户端要从服务器中读取文档时，当点击网页上的链接或者通过在浏览器的地址栏输入网址来浏览网页的，使用的都是GET方式。GET方法要求服务器将URL定位的资源放在响应报文的数据部分，会送给客户端。使用GET方法时，请求参数和对应的值附加在URL后面，利用一个问号‘？’代表URL的结尾与请求参数的开始，传递参数长度受限制。例如，/index.jsp?id=100&op=bind。通过GET方式传递的数据直接放在地址中，所以GET方式的请求一般不包含“请求内容”部分，请求数据以地址的形式表现在请求行。地址中‘？’之后的部分就是通过GET发送的请求数据，各个数据之间用‘&’符号隔开。显然这种方式不适合传送私密数据。另外，由于不同的浏览器对地址的字符限制也有所不同，一半最多只能识别1024个字符，所以如果需要传送大量数据的时候，也不适合使用GET方式。如果数据是英文字母/数字，原样发送；如果是空格，转换为+；如果是中文/其他字符，则直接把字符串用BASE64加密，得出：%E4%BD%A0%E5%A5%BD，其中%XX中的XX为该符号以16进制表示的ASCII。

（2）POST

       允许客户端给服务器提供信息较多。POST方法将请求参数封装在HTTP请求数据中，以名称/值的形式出现，可以传输大量数据，这样POST方式对传送的数据大小没有限制，而且也不会显示在URL中。POST方式请求行中不包含数据字符串，这些数据保存在“请求内容”部分，各数据之间也是使用‘&’符号隔开。POST方式大多用于页面的表单中。因为POST也能完成GET的功能，因此多数人在设计表单的时候一律都使用POST方式，其实这是一个误区。GET方式也有自己的特点和优势，我们应该根据不同的情况来选择是使用GET还是使用POST。

例子如下
```
// [HttpPost, Route("{id}")]
        // //评论实现
        // //使用post请求，因为已经打开了文章，所以评论的路由是后缀名有文章的，应该是如下这样
        // public dynamic Post(int id,RefreshTokenDTO refresh,CreateComment createComment)
        // {
        //     var UserId = TokenHelper.UserValidateToken(_ToKenParmeter, refresh);
        //     var insertComments = new Comments
        //         {
        //             UserId=int.Parse(UserId),
        //             ArticleId=id,
        //             CommentContent=createComment.CommentContent                     
        //         };
        //         _comments.Insert(insertComments);

        //     return "1";
        // }

```

这样写是不行的，post请求输入可以传大量数据，但是最好就1个参数，不然会报错,以下是正确写法

```
 [HttpPost, Route("{id}")]
        // 评论实现
        // 使用post请求，因为已经打开了文章，所以评论的路由是后缀名有文章的id，应该是如下这样
        public dynamic Post(int id,CreateComment createComment)
        {
           
            var insertComments = new Comments
                {
                    UserId=createComment.UserId,
                    ArticleId=id,
                    CommentContent=createComment.CommentContent                     
                };
                _commentsRepository.Insert(insertComments);

            return  new
                {
                    Code = 104,
                    Data = insertComments,
                    Msg = "添加评论成功"
                };
        }

```