using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS_data.api.Entity;

namespace CMS_data.api.IRepository
{
    public interface  IRepository<T> where T : BaseEntity
    {
        IQueryable<T> table{get;}

        T GetById(object id);

        void Insert(T entity);

        Task InsertAsync(T entity);

        void InsertBulk(IEnumerable<T> entities);

         void InsertBulkAsync(IEnumerable<T> entities);

         void Delete(object id);

         void DeleteBulk(IEnumerable<object> Ids);

         void update(T entity);

         void updateBulk(IEnumerable<T> entities);
    }
}