using System.Collections.Generic;
using CMS_data.api.Entity;
using Microsoft.AspNetCore.Mvc;


namespace CMS_data.api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class  UserController :ControllerBase
    {
         private  IRepository<Articles> 
        
        [HttpPost]
        public dynamic Post(CreateUser newUser)
        {
            var username = newUser.Username.Trim().Length==0 ? null: newUser.Username.Trim();
            var password = newUser.Password.Trim().Length==0 ? null: newUser.Password.Trim();
            var remarks = newUser.remarks;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return JsonHelper.Serialize(new {
                    Code = 104,
                    Data = "",
                    Msg = "用户名或密码不能为空"
                });
               
            }


            var user = new Users
            {
                UserName = newUser.Username,
                PassWord = newUser.Password,
                Remarks= newUser.remarks
            };

            _usersRepository.Insert(user);
            return JsonHelper.Serialize(new 
            {
                Code = 1000,
                Data = user,
                Msg = "创建用户成功^_^"
            }); 
        }


    }

    
}