using Microsoft.EntityFrameworkCore;
using CMS_data.api.Entity;
using System;

namespace CMS_data.api.Database
{
    public class CMSdataDbHerder : DbContext
    {

        public CMSdataDbHerder(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Users> Users { get; set; }
        public DbSet<Admin> Admin { get; set; }
        public DbSet<Articles> Articles { get; set; }
        public DbSet<Comments> Comments { get; set; }
        public DbSet<Label> Label { get; set; }
       public DbSet<UserLike> UserLike { get; set; }

       public DbSet<UserRead> UserRead { get; set; }
       public DbSet<PowerInfo> PowerInfo{get;set;}
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"server=.;database=CMSdata;uid=sa;pwd=123456;");

        }

         protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 针对Blog实体添加种子数据
            modelBuilder.Entity<Label>().HasData(
                new Label()
                {
                    // Id字段要赋值，否则会报错
                    Id=1,
                    LabelName="新闻热点",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1

                },
                new Label()
                {
                    Id=2,
                    LabelName="公司动态",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Label()
                {
                    Id=3,
                    LabelName="大神风采",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                // 新增加一条数据
                new Label()
                {
                    Id=4,
                    LabelName="了解我们",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                }
                
            );

            modelBuilder.Entity<PowerInfo>().HasData(
                new PowerInfo()
                {
                    // Id字段要赋值，否则会报错
                    Id=1,
                    PowerName="King",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1

                },
                new PowerInfo()
                {
                    Id=2,
                    PowerName="Queen",
                   IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new PowerInfo()
                {
                    Id=3,
                    PowerName="Knight",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                // 新增加一条数据
                new PowerInfo()
                {
                    Id=4,
                     PowerName="Pawn",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                }
            );

            
            modelBuilder.Entity<Users>().HasData(
                
                new Users()
                {
                    Id=1,
                    UserName="admin1",
                    PassWord="113",
                    PersonalizedName="轻轻的我来了",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Users()
                {
                    Id=2,
                    UserName="admin2",
                    PassWord="113",
                    PersonalizedName="轻轻的我来了",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Users()
                {
                    Id=3,
                    UserName="admin3",
                    PassWord="113",
                    PersonalizedName="轻轻的我来了",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Users()
                {
                    Id=4,
                    UserName="admin4",
                    PassWord="113",
                    PersonalizedName="轻轻的我来了",
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                }
                
                
            );
             modelBuilder.Entity<Admin>().HasData(
                new Admin()
                {
                    Id=1,
                    UserName="king",
                    PassWord="113",
                    PowerInfoId=1,
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Admin()
                {
                    Id=2,
                    UserName="Queen",
                    PassWord="113",
                    PowerInfoId=2,
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Admin()
                {
                    Id=3,
                    UserName="Knight",
                    PassWord="113",
                    PowerInfoId=3,
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                },
                new Admin()
                {
                    Id=4,
                    UserName="pawn",
                    PassWord="113",
                    PowerInfoId=4,
                    IsActived=true,
                    IsDeleted=false,
                    CreatedTime=DateTime.Now,
                    UpdatedTime=DateTime.Now,
                    DisplayOrder=1
                }

             );
            base.OnModelCreating(modelBuilder);
        }
        
    }
}