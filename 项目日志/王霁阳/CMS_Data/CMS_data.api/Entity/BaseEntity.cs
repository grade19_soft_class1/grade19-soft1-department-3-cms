using System;
using System.ComponentModel.DataAnnotations;

namespace CMS_data.api.Entity
{
    public class BaseEntity
    {
        public int Id { get; set; }

         /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActived { get; set; }

         /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDeleted { get; set; }

         /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }

         /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdatedTime { get; set; }

         /// <summary>
        /// 版本
        /// </summary>
        public int DisplayOrder { get; set; }

         /// <summary>
        /// 备注
        /// </summary>
        
        [MaxLength(50)]
        public string Remarks { get; set; }
    }
}