/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/7/29 9:55:34                            */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Admin') and o.name = 'FK_ADMIN_REFERENCE_POWERINF')
alter table Admin
   drop constraint FK_ADMIN_REFERENCE_POWERINF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Articles') and o.name = 'FK_ARTICLES_REFERENCE_USERS')
alter table Articles
   drop constraint FK_ARTICLES_REFERENCE_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Articles') and o.name = 'FK_ARTICLES_REFERENCE_LABEL')
alter table Articles
   drop constraint FK_ARTICLES_REFERENCE_LABEL
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Articles') and o.name = 'FK_ARTICLES_REFERENCE_ADMIN')
alter table Articles
   drop constraint FK_ARTICLES_REFERENCE_ADMIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Comments') and o.name = 'FK_COMMENTS_REFERENCE_USERS')
alter table Comments
   drop constraint FK_COMMENTS_REFERENCE_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Comments') and o.name = 'FK_COMMENTS_REFERENCE_ARTICLES')
alter table Comments
   drop constraint FK_COMMENTS_REFERENCE_ARTICLES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserLike') and o.name = 'FK_USERLIKE_REFERENCE_ARTICLES')
alter table UserLike
   drop constraint FK_USERLIKE_REFERENCE_ARTICLES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserLike') and o.name = 'FK_USERLIKE_REFERENCE_USERS')
alter table UserLike
   drop constraint FK_USERLIKE_REFERENCE_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRead') and o.name = 'FK_USERREAD_REFERENCE_ARTICLES')
alter table UserRead
   drop constraint FK_USERREAD_REFERENCE_ARTICLES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRead') and o.name = 'FK_USERREAD_REFERENCE_USERS')
alter table UserRead
   drop constraint FK_USERREAD_REFERENCE_USERS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Admin')
            and   type = 'U')
   drop table Admin
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Articles')
            and   type = 'U')
   drop table Articles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Comments')
            and   type = 'U')
   drop table Comments
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Label')
            and   type = 'U')
   drop table Label
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PowerInfo')
            and   type = 'U')
   drop table PowerInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserLike')
            and   type = 'U')
   drop table UserLike
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserRead')
            and   type = 'U')
   drop table UserRead
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Users')
            and   type = 'U')
   drop table Users
go

/*==============================================================*/
/* Table: Admin                                                 */
/*==============================================================*/
create table Admin (
   Id                   int                  identity,
   AdminName            nvarchar(50)         null,
   PassWord             nvarchar(50)         null,
   PowerInfoId          int                  null,
   constraint PK_ADMIN primary key (Id)
)
go

/*==============================================================*/
/* Table: Articles                                              */
/*==============================================================*/
create table Articles (
   Id                   int                  not null,
   ArticlesTitle        nvarchar(50)         null,
   ArticleContent       nvarchar(Max)        null,
   ArticleSynopsis      nvarchar(50)         null,
   UserId               int                  null,
   LabelId              int                  null,
   "Check"              bit                  null,
   AdminId              int                  null,
   recommend            bit                  null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null,
   constraint PK_ARTICLES primary key (Id)
)
go

/*==============================================================*/
/* Table: Comments                                              */
/*==============================================================*/
create table Comments (
   Id                   int                  not null,
   UserId               int                  null,
   ArticleId            int                  null,
   CommentContent       nvarchar(500)        null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null,
   constraint PK_COMMENTS primary key (Id)
)
go

/*==============================================================*/
/* Table: Label                                                 */
/*==============================================================*/
create table Label (
   Id                   int                  identity,
   LabelName            nvarchar(50)         null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null,
   constraint PK_LABEL primary key (Id)
)
go

/*==============================================================*/
/* Table: PowerInfo                                             */
/*==============================================================*/
create table PowerInfo (
   Id                   int                  identity,
   PowerName            nvarchar(50)         null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null,
   constraint PK_POWERINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: UserLike                                              */
/*==============================================================*/
create table UserLike (
   Id                   int                  not null,
   UserId               int                  null,
   ArticleId            int                  null,
   IfLike               bit                  null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null,
   constraint PK_USERLIKE primary key (Id)
)
go

/*==============================================================*/
/* Table: UserRead                                              */
/*==============================================================*/
create table UserRead (
   UserId               int                  null,
   ArticleId            int                  null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null
)
go

/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/
create table Users (
   Id                   int                  not null,
   UserName             nvarchar(50)         null,
   PassWord             nvarchar(50)         null,
   PersonalizedName     nvarchar(100)        null,
   IsActived            bit                  null,
   IsDeleted            bit                  null,
   DateTime             datetime             null,
   UpdatedTime          datetime             null,
   DisplayOrder         int                  null,
   Remarks              nvarchar(50)         null,
   constraint PK_USERS primary key (Id)
)
go

alter table Admin
   add constraint FK_ADMIN_REFERENCE_POWERINF foreign key (PowerInfoId)
      references PowerInfo (Id)
go

alter table Articles
   add constraint FK_ARTICLES_REFERENCE_USERS foreign key (UserId)
      references Users (Id)
go

alter table Articles
   add constraint FK_ARTICLES_REFERENCE_LABEL foreign key (LabelId)
      references Label (Id)
go

alter table Articles
   add constraint FK_ARTICLES_REFERENCE_ADMIN foreign key (AdminId)
      references Admin (Id)
go

alter table Comments
   add constraint FK_COMMENTS_REFERENCE_USERS foreign key (UserId)
      references Users (Id)
go

alter table Comments
   add constraint FK_COMMENTS_REFERENCE_ARTICLES foreign key (ArticleId)
      references Articles (Id)
go

alter table UserLike
   add constraint FK_USERLIKE_REFERENCE_ARTICLES foreign key (ArticleId)
      references Articles (Id)
go

alter table UserLike
   add constraint FK_USERLIKE_REFERENCE_USERS foreign key (UserId)
      references Users (Id)
go

alter table UserRead
   add constraint FK_USERREAD_REFERENCE_ARTICLES foreign key (ArticleId)
      references Articles (Id)
go

alter table UserRead
   add constraint FK_USERREAD_REFERENCE_USERS foreign key (UserId)
      references Users (Id)
go

