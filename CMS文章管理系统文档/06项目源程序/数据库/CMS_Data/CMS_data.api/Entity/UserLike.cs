namespace CMS_data.api.Entity
{
     public class UserLike : BaseEntity
     {
        /// <summary>
        /// 用户Id
        /// </summary>
          
        /// <summary>
        /// 文章Id
        /// </summary>
         
        /// <summary>
        /// 是否点赞
        /// </summary>

          public bool IfLike { get; set; }

          public virtual Users User { get; set; }

          public virtual Articles Article { get; set; }
     }
}