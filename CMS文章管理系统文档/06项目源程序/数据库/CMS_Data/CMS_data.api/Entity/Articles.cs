using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS_data.api.Entity
{
    public class Articles : BaseEntity
    {

         /// <summary>
        /// 创建者
        /// </summary>
        public int UserId{get;set;}

        /// <summary>
        /// 文章标题
        /// </summary>
        
        [MaxLength(50)]
        public string ArticlesTitle{get;set;}
        
        /// <summary>
        /// 文章内容
        /// </summary>
        [MaxLength(5000)]
        public string ArticleContent {get;set;}

        /// <summary>
        /// 文章简介
        /// </summary>
        [MaxLength(50)]
        public string ArticleSynopsis{get;set;}

        /// <summary>
        /// 标签外键Id
        /// </summary>


        /// <summary>
        /// 是否审核
        /// </summary>

        public bool  Check {get;set;}

         /// <summary>
        /// 审核人
        /// </summary>


        /// <summary>
        /// 是否推荐
        /// </summary>
        
        public bool  recommend {get;set;}

        public virtual Users User {get;set;}

        public virtual Label Label {get;set;}

        public virtual IEnumerable<Comments> Comments { get; set; }

        public virtual IEnumerable<UserLike> UserLike { get; set; }

        public virtual IEnumerable<UserRead> UserRead { get; set; }

        public virtual Admin Admin {get;set;}

        

    }
}