using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS_data.api.Entity
{
    public class Label : BaseEntity
    {

        /// <summary>
        /// 标签名字
        /// </summary>
        [MaxLength(20)]
       public string LabelName{get;set;}

        public virtual IEnumerable<Articles> Article { get; set; }

    }
}