namespace CMS_data.api.Entity
{
     public class UserLike : BaseEntity
     {
        /// <summary>
        /// 用户Id
        /// </summary>
          public int UserId{get;set;}

        /// <summary>
        /// 文章Id
        /// </summary>
          public int ArticleId{get;set;}
         
        /// <summary>
        /// 是否点赞
        /// </summary>

          public bool IfLike { get; set; }

     
     }
}