using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace CMS_data.api.Entity
{
    public class Admin:BaseEntity
    {
       
         /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(50)]
        public string UserName { get; set; }

         /// <summary>
        /// 密码
        /// </summary>
        [MaxLength(50)]
        public string PassWord { get; set; }

         /// <summary>
        /// 权限
        /// </summary>

        public int PowerInfoId{get;set;}
       

    }
}