using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace  CMS_data.api.Entity
{
    public class Users : BaseEntity
    {
         /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(50)]
        public string UserName { get; set; }

         /// <summary>
        /// 密码
        /// </summary>
        [MaxLength(50)]
        public string PassWord { get; set; }

         /// <summary>
        /// 个性签名
        /// </summary>
        [MaxLength(100)]
        public string PersonalizedName { get; set; }


    }
}