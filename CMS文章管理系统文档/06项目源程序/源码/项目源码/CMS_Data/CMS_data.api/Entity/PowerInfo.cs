using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS_data.api.Entity
{
    public class PowerInfo:BaseEntity
    {

        /// <summary>
        /// 权限名
        /// </summary>
        [MaxLength(20)]
       public string PowerName{get;set;}


    }
}