namespace CMS_data.api.Entity
{
     public class UserRead : BaseEntity
     {
        /// <summary>
        /// 用户Id
        /// </summary>
          public int UserId{get;set;}

        /// <summary>
        /// 文章Id
        /// </summary>
          public int ArticleId{get;set;}

     }
}