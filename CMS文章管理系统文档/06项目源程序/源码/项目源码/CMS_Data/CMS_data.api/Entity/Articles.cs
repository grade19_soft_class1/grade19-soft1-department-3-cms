using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CMS_data.api.Params;

namespace CMS_data.api.Entity
{
    public class Articles : BaseEntity
    {

         /// <summary>
        /// 创建者
        /// </summary>
        public int AdminId{get;set;}

        /// <summary>
        /// 文章标题
        /// </summary>
        
        [MaxLength(50)]
        public string ArticlesTitle{get;set;}
        
        /// <summary>
        /// 文章内容
        /// </summary>
        [MaxLength(5000)]
        public string ArticleContent {get;set;}

        /// <summary>
        /// 文章简介
        /// </summary>
        [MaxLength(50)]
        public string ArticleSynopsis{get;set;}

        /// <summary>
        /// 标签外键Id
        /// </summary>
        public int LabelId{get;set;}

          /// <summary>
        /// 文章图片路径
        /// </summary>

        public string ArticleImage{get;set;}

        /// <summary>
        /// 是否推荐
        /// </summary>
        
        public bool  recommend {get;set;}

        
        public virtual Label label{get;set;}
        public virtual Admin Admin{get;set;}
    }
}