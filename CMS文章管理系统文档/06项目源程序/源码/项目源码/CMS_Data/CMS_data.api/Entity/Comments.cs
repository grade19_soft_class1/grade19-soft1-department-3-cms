namespace CMS_data.api.Entity
{
    public class Comments:BaseEntity
    {
         /// <summary>
        /// 用户Id
        /// </summary>
          public int UserId{get;set;}

        /// <summary>
        /// 文章Id
        /// </summary>
          public int ArticleId{get;set;}

        /// <summary>
        /// 评论
        /// </summary>

        public string CommentContent {get;set;}
    }
}