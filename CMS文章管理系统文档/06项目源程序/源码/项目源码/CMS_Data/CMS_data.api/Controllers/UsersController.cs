using Microsoft.AspNetCore.Mvc;
using CMS_data.api.IRepository;
using CMS_data.api.Entity;
using System.Linq;
using CMS_data.api.Params;

using Microsoft.Extensions.Configuration;
using CMS_data.api.Utils;
using Microsoft.AspNetCore.Authorization;

namespace CMS_data.api.Controllers
{
    // [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IConfiguration _configuration;
        private IRepository<Users> _usersRepository;

        private TokenParameter _ToKenParmeter;
        public UsersController(IRepository<Users> usersRepository, IConfiguration configuration)
        {
            _configuration = configuration;
            _usersRepository = usersRepository;
            _ToKenParmeter = configuration.GetSection("tokenParameter").Get<TokenParameter>();
        }

        [HttpGet]
        public dynamic Get([FromQuery]QueryWithPager queryWithPager)
        {
            // get请求默认从url中获取参数，如果需要使用实体接收参数，需要FromQuery特性
                //获取前端传来用于查询的字段
                var keyWord=string.IsNullOrEmpty(queryWithPager.keyWord)?"":queryWithPager.keyWord.Trim();

                 var users = _usersRepository.table;
                //前端传来页码数，根据页码数来分开查找
                var pageIndex=queryWithPager.pageIndex;
                var pageSize=queryWithPager.pageSize;
                //获取前端传来用于查询的字段
                //判断前端传来的是不是空的

                if(!string.IsNullOrEmpty(keyWord))
                {
                   users = users.Where(x => x.UserName.Contains(keyWord));
                }

                //查询所有的文章并且获取标签名和管理员名
                var u=users.Skip((pageIndex-1)*pageSize).Take(pageSize).ToList();
                //查询

                var res =  new
            {
                Code = 1000,
               Data = new {Data = u,Pager =new {pageIndex,pageSize,rowsTotal=users.Count()}},
                Msg = "获取用户列表成功^_^"
            };
            var returnString = JsonHelper.Serialize(res);
            return returnString;
        }

        [HttpGet("{id}")]
        public dynamic Get(int id)
        {
            var user = _usersRepository.GetById(id);
            return new
            {
                Code = 1000,
                Data = user,
                Msg = "获取指定用户成功^_^"
            };
        }

        
        [HttpPost]
        public dynamic Post( CreateUser newUser)
        {
            var username = newUser.Username.Trim().Length==0 ? null: newUser.Username.Trim();
            var password = newUser.Password.Trim().Length==0 ? null: newUser.Password.Trim();
            newUser.remarks="3";
            var remarks = newUser.remarks;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return JsonHelper.Serialize(new {
                    Code = 104,
                    Data = "",
                    Msg = "用户名或密码不能为空"
                });
               
            }


            var user = new Users
            {
                UserName = newUser.Username,
                PassWord = newUser.Password,
                Remarks= newUser.remarks
            };

            _usersRepository.Insert(user);
            return JsonHelper.Serialize(new 
            {
                Code = 1000,
                Data = user,
                Msg = "创建用户成功^_^"
            }); 
        }
        






        [HttpPut("{id}")]
        public dynamic Put(int id, CreateUser updateUser)
        {
            var username = updateUser.Username.Trim().Length==0 ? null: updateUser.Username.Trim();
            var password = updateUser.Password.Trim().Length== 0 ? "": updateUser.Password.Trim();
            var remarks = updateUser.remarks;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return JsonHelper.Serialize(new {
                    Code = 104,
                    Data = "",
                    Msg = "用户名或密码不能为空"
                });

            }


            var user = _usersRepository.GetById(id);

            if (user == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "要更新的用户不存在，请确认后重试=_="
                };
            }

            user.UserName = updateUser.Username;
            user.PassWord = updateUser.Password;
            user.Remarks=updateUser.remarks;

            _usersRepository.update(user);

           return JsonHelper.Serialize(new 
            {
                Code = 1000,
                Data = user,
                Msg = "修改用户成功^_^"
            }); 
        }


        //逻辑删除用户
        [HttpDelete("{id}")]
        public dynamic Delete(int id)
        {
             //找到这个用户
            var users = _usersRepository.GetById(id);

            if(users==null)
            {
                return new{
                Code = 1000,
                Data = "",
                Msg = "没有这个用户"
                };
            }
            users.IsDeleted=true;
            _usersRepository.update(users);
            return new
            {
                Code = 1000,
                Msg = "删除用户成功^_^"
            };
        }

        
        [HttpPost, Route("token")]
        public dynamic GetToken(CreateUser newUser)
        {
            var username = newUser.Username.Trim();
            var password = newUser.Password.Trim();
            var user = _usersRepository.table.Where(x => x.UserName == username && x.PassWord == password).FirstOrDefault();

            if (user == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "用户名或密码不正确，请确认后重试"
                };
            }
            if(!user.IsActived)
            {
                    return new
                {
                    Code = 900,
                    Data = "",
                    Msg = "用户已被禁用"
                };
            }
            if(user.IsDeleted)
            {
                return new
                {
                    Code = 900,
                    Data = "",
                    Msg = "用户已被删除，请联系管理员"
                };
            }
            var token = TokenHelper.GenerateToken(_ToKenParmeter, user.UserName);
            var refreshToken = "112358";

            return new
            {
                Code = 1000,
                Data = new { Token = token,refreshToken=refreshToken,UserId=user.Id.ToString(),UserName=user.UserName },
                Msg = "用户登录成功^_^"
            };
            
        }

        [AllowAnonymous]
        [HttpPost, Route("refreshtoken")]
        public dynamic RefreshToken(RefreshTokenDTO refresh)
        {
            var username = TokenHelper.ValidateToken(_ToKenParmeter, refresh);

            if (string.IsNullOrEmpty(username))
            {
                return new { Code = 1002, Data = "", Msg = "token验证失败" };
            }

            var token = TokenHelper.GenerateToken(_ToKenParmeter, username);
            var refreshToken = "112358";

            return new
            {
                Code = 1000,
                Data = new { Token = token, refreshToken = refreshToken },
                Msg = "刷新token成功^_^"
            };
        }
        [HttpPut, Route("{id}/IsActived")]
        //用户禁用
        public dynamic IsActived(int id)
        {
            //找到这个用户
            var users = _usersRepository.GetById(id);

            if(users==null)
            {
                return new{
                Code = 1000,
                Data = "",
                Msg = "没有这个用户"
                };
            }
            if(users.IsActived)
            {
                users.IsActived=false;
            }else
            {
                users.IsActived=true;
            }
            _usersRepository.update(users);

           return JsonHelper.Serialize(new 
            {
                Code = 1000,
                Data = users,
                Msg = "修改用户成功^_^"
            }); 

        }

    }
}