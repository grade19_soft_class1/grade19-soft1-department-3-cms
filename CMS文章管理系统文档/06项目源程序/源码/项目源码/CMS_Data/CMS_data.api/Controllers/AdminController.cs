using Microsoft.AspNetCore.Mvc;
using CMS_data.api.IRepository;
using CMS_data.api.Entity;
using System.Linq;
using CMS_data.api.Params;
using Microsoft.Extensions.Configuration;
using CMS_data.api.Utils;

namespace CMS_data.api.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AdminController : ControllerBase
    {
        private IConfiguration _configuration;
        private IRepository<Admin> _adminRepository;
        private TokenParameter _ToKenParmeter;

        //Admin接口
        public AdminController(IRepository<Admin> AdminRepository, IConfiguration configuration)
        {
            _configuration = configuration;
            _adminRepository = AdminRepository;
            _ToKenParmeter = configuration.GetSection("tokenParameter").Get<TokenParameter>();
        }

        [HttpGet("{id}")]
        public dynamic GetDynamic(int id)
        {
            var user = _adminRepository.GetById(id);
            return new
            {
                Code = 1000,
                Data = user,
                Msg = "获取指定用户成功"
            };
        }

        [HttpPost]
        public dynamic Post(CreateAdmin newAdminUser)
        {
            var username = newAdminUser.RootName.Trim().Length == 0 ? null : newAdminUser.RootName.Trim();
            var password = newAdminUser.Pass.Trim().Length == 0 ? null : newAdminUser.Pass.Trim();
            newAdminUser.PowerInfoId = 3;
            var powerInfoId = newAdminUser.PowerInfoId;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return JsonHelper.Serialize(new
                {
                    Code = 104,
                    Data = "",
                    Msg = "用户名或密码不能为空"
                });
            }
            var adminUser = new Admin
            {
                UserName = newAdminUser.RootName,
                PassWord = newAdminUser.Pass,
                PowerInfoId = newAdminUser.PowerInfoId
            };
            _adminRepository.Insert(adminUser);
            return JsonHelper.Serialize(new
            {
                Code = 1000,
                Data = adminUser,
                Msg = "创建用户成功"
            });
        }

        [HttpPost, Route("token")]
        public dynamic GetToken(CreateAdmin newAdminUser)
        {
            var username = newAdminUser.RootName.Trim();
            var password = newAdminUser.Pass.Trim();
            var user = _adminRepository.table.Where(x => x.UserName == username && x.PassWord == password).FirstOrDefault();
            if (user == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "用户或密码不正确，请确认后重试"
                };
            }
            var token = TokenHelper.GenerateToken(_ToKenParmeter, user);
            var refreshToken = "112358";
           
            return new
            {
                Code = 1000,
                Data = new {Token = token,refreshToken=refreshToken,AdminId = user.Id,AdminName=user.UserName},
                Msg = "用户登录成功"
            };
        }
        // [HttpPut("{id}")]
        // public dynamic Put(int id,CreateAdmin updateAdminUser)
        // {
        //     var username = updateAdminUser.UserName.Trim().Length==0?null:updateUser.Username.Trim();
        //     var password = updateAdminUser.PassWord.Trim().Length==0?null:updateAdminUser.Password.Trim();
        //     var powerInfoId = updateAdminUser.powerInfoId;
        //     if(string.IsNullOrEmpty(username)||string.IsNullOrEmpty(password)){
        //         return JsonHelper.Serialize(new{
        //             Code = 104,
        //             Data = "",
        //             Msg = "用户名或密码不能为空"
        //         });
        //         }
        //                         var user = _adminRepository.GetById(id);
        //         if(user == null){
        //             return new{
        //                 Code = 104,
        //                 Data = "",
        //                 Msg = "要更新的用户不存在，请确认后重试"
        //             };
        //     }
        //     user.UserName = updateAdminUser.username;
        //     user.PassWord = updateAdminUser.Password;
        //     user.powerInfoId = updateAdminUser.powerInfoId;

        //     _adminRepository.update(user);
        //     return JsonHelper.Serialize(new{
        //         Code = 1000,
        //         Data = user,
        //         Msg = "修改用户成功"
        //     });
        // }
        // [HttpDelete("{id}")]
        // public dynamic Delete(int id){
        //     _adminRepository.Delete(id);
        //     return new{
        //         Code = 1000,
        //         Data = "",
        //         Msg = "删除成功"
        //     };
        // }
        //     [HttpPost, Route("token")]
        //     public dynamic GetToken(CreateUser newUser){
        //         var username = newAdminUser.Username.Trim();
        //         var password = newAdminUser.Password.Trim();
        //         var user = _adminRepository.table.Where(x => x.UserName == username && x.PassWord == password).FirstOrDefault();
        //         if(user == null){
        //             return new{
        //                 Code = 104,
        //                 Data = "",
        //                 Msg = "用户名或密码不正确，请确认后重试"
        //             };
        //         }
        //         var token = TokenHelper.GenerateToken(_ToKenParmeter,user);
        //         var refreshToken = "112358";
        //         return new{
        //             Code = 1000,
        //             Data = new {Token = token,refreshToken = refreshToken,UserId=user.Id},
        //             Msg = "用户登录成功"
        //         }
        //     }
        // }

              [HttpDelete("{id}")]
        public dynamic Delete(int id)
        {
            var admin = _adminRepository.GetById(id);
            if (admin != null)
            {
                
                _adminRepository.Delete(id);
                return new
                {
                    Code = 1000,
                    Msg = string.Format("删除指定id为{0}的管理员成功", id)
                };
            }
            else
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "指定Id的管理员不存在，请确认后重试"
                };
            }

        }
    }
}