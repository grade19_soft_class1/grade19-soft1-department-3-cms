using CMS_data.api.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using CMS_data.api.Entity;
using System.Linq;

namespace CMS_data.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
      public class LabelController : ControllerBase
    {
        private IConfiguration _configuration;

        private IRepository<Label> _labelRepository;

         private IRepository<Articles> _articlesRepository;

        public LabelController(IRepository<Label> labelRepository,IConfiguration configuration,IRepository<Articles> articlesRepository)
        {
            _configuration = configuration;
            _labelRepository=labelRepository;
            _articlesRepository=articlesRepository;
        }

        //获取文章的列表
         [HttpGet]
        public dynamic GetLabel()
        {
            //获取文章的所以标签
            var labels = _labelRepository.table;
            //返回给前端
            return new
            {
                core = 1000,
                res = labels,
                Msg= "有问题"
            };
        }

        [HttpGet,Route("{id}/Articles")]
        public dynamic GetLabelArticles(int id)
        {
            //根据点击的Id来查询文章
            var labelArticle = _articlesRepository.table.Where(x=>x.LabelId==id).ToList();

              return new
                {
                    Code = 1000,
                    Data = labelArticle,
                    Msg = "修改成功"
                };
        }
    }
}