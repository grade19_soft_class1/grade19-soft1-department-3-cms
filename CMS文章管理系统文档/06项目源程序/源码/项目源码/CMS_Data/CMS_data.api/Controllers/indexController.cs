using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CMS_data.api.Entity;
using CMS_data.api.IRepository;
using CMS_data.api.Params;
using CMS_data.api.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace CMS_data.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class indexController : ControllerBase
    {
         
        private IConfiguration _configuration;
        private IRepository<Articles> _articlesRepository;

        private IRepository<UserRead> _userReadRepository;

        //图片构造注入
        private readonly PictureOptions _pictureOptions;

        private IRepository<Comments> _commentsRepository;

        private IRepository<UserLike> _userLikeRepository;

        private IRepository<Admin> _adminRepository;

        private IRepository<Label> _labelRepository;

        //依赖注入
        public indexController(IRepository<Articles> articlesRepository, IConfiguration configuration,IRepository<UserRead> userReadRepository,IRepository<Comments> CommentsRepository,
        IRepository<UserLike> userLikeRepository,IRepository<Admin> adminRepository,IRepository<Label> labelRepository,IOptions<PictureOptions> options)
        {
            _configuration = configuration;
            _articlesRepository = articlesRepository;
            _userReadRepository=userReadRepository;
            _commentsRepository=CommentsRepository;
            _userLikeRepository=userLikeRepository;
            _adminRepository=adminRepository;
            _labelRepository=labelRepository;
            //图片构造注入
            _pictureOptions=options.Value;
        }

        //小编推荐，1周评论最多，一周点赞最多
        public dynamic getIndex()
        {
            //小编推荐
            //获取系统7天前的时间
            DateTime dt = DateTime.Now.AddDays(-7);


            //找到一周以内的文章日期
            var article = _articlesRepository.table.Where(x =>x.UpdatedTime>dt).ToList();

            //找到阅读表
            var userRead = _userReadRepository.table.ToList();

            var userLike =_userLikeRepository.table.ToList();

            //找到评论表
            var userComments = _commentsRepository.table.ToList();

            //找到一周以内的小编推荐
            var recommend = article.Where(x =>x.recommend == true).ToList();

            //找到一周以内阅读最多
            //首先，要关联到阅读表
            //找出有多少跳评论，返回
            var Read = article.Select(x =>
                {
                    var userReadnum = userRead.Count(y => y.ArticleId == x.Id);

                    return new {
                        id = x.Id,
                        updatedTime=x.UpdatedTime,
                        articlesTitle=x.ArticlesTitle,
                        ArticleSynopsis=x.ArticleSynopsis,
                        LabelId=x.LabelId,
                        userReadnum=userReadnum
                    };
                }
            );
            //找到阅读量大于0的，并且排序
            var userReadnum=Read.Where(x=>x.userReadnum>0).OrderByDescending(x=>x.userReadnum).ToList();

            //评论排序
            //找到一周以内阅读最多
            //首先，要关联到阅读表
            //找出有多少跳评论，返回

             var Comments = article.Select(x =>
                {
                    var UseruserCommentsnum = userComments.Count(y => y.ArticleId == x.Id);

                    return new {
                        id = x.Id,
                        UpdatedTime=x.UpdatedTime,
                        ArticlesTitle=x.ArticlesTitle,
                        ArticleSynopsis=x.ArticleSynopsis,
                        LabelId=x.LabelId,
                        UseruserCommentsnum=UseruserCommentsnum
                    };
                }
            );

              var userCommentnum=Comments.Where(x=>x.UseruserCommentsnum>0).OrderByDescending(x=>x.UseruserCommentsnum).ToList();


            //找到对应的数据
             var Like = article.Select(x =>
                {
                    var UseruserLikesnum = userLike.Count(y => y.ArticleId == x.Id);

                    return new {
                        id = x.Id,
                        UpdatedTime=x.UpdatedTime,
                        ArticlesTitle=x.ArticlesTitle,
                        ArticleSynopsis=x.ArticleSynopsis,
                        LabelId=x.LabelId,
                        UseruserLikesnum=UseruserLikesnum
                    };
                }
            );


              //现在做主页的文章显示了
                var articleIndex = article.Select(x=>
                {
                    var indexRead = Read.Where(y=>y.id==x.Id);
                    var indexComments = Comments.Where(y=>y.id==x.Id);
                    var indexLike = Like.Where(y=> y.id==x.Id);
                    var indexCommentRun = userComments.Where(y=>y.ArticleId==x.Id);
                    return new
                    {
                        id = x.Id,
                        UpdatedTime=x.UpdatedTime,
                        ArticlesTitle=x.ArticlesTitle,
                        ArticleSynopsis=x.ArticleSynopsis,
                        ArticleContent=x.ArticleContent,
                        LabelId=x.LabelId,
                        indexRead=indexRead,
                        indexComments=indexComments,
                        indexLike=indexLike,
                        indexCommentRun=indexCommentRun
                    };
                });


            return new {
                    Code = 1000,
                    Data = new
                    {
                        recommend=recommend,
                        userReadnum=userReadnum,
                        userCommentnum=userCommentnum,
                        articleIndex=articleIndex
                    },
                    Msg = "拉取成功"
            };
        
        }

        


    }   
}
