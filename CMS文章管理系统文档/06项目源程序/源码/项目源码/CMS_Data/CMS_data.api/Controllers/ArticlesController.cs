using Microsoft.AspNetCore.Mvc;
using CMS_data.api.IRepository;
using CMS_data.api.Entity;
using System.Linq;
using CMS_data.api.Params;
using Microsoft.Extensions.Configuration;
using CMS_data.api.Utils;
using Z.EntityFramework.Plus;
using System.Xml.Schema;
using Microsoft.EntityFrameworkCore;
using MS_data.api.Params;

namespace CMS_data.api.Controllers
{


    [ApiController]
    [Route("[controller]")]
    public class ArticlesController : ControllerBase
    {
       private IConfiguration _configuration;
        private IRepository<Articles> _articlesRepository;

        private IRepository<UserRead> _userReadRepository;

        private TokenParameter _ToKenParmeter;

        private IRepository<Comments> _commentsRepository;

        private IRepository<UserLike> _userLikeRepository;

        private IRepository<Admin> _adminRepository;

        private IRepository<Label> _labelRepository;

        public ArticlesController(IRepository<Articles> articlesRepository, IConfiguration configuration,IRepository<UserRead> userReadRepository,IRepository<Comments> CommentsRepository,
        IRepository<UserLike> userLikeRepository,IRepository<Admin> adminRepository,IRepository<Label> labelRepository
        )
        {
            _configuration = configuration;
            _articlesRepository = articlesRepository;
            _userReadRepository=userReadRepository;
            _commentsRepository=CommentsRepository;
            _userLikeRepository=userLikeRepository;
            _adminRepository=adminRepository;
            _labelRepository=labelRepository;
            _ToKenParmeter = configuration.GetSection("tokenParamerter").Get<TokenParameter>();
        }
        [HttpGet]
         public dynamic Get([FromQuery]QueryWithPager queryWithPager)
            {
                 // get请求默认从url中获取参数，如果需要使用实体接收参数，需要FromQuery特性
                //获取前端传来用于查询的字段
                var keyWord=string.IsNullOrEmpty(queryWithPager.keyWord)?"":queryWithPager.keyWord.Trim();

                 var articles = _articlesRepository.table;
                //前端传来页码数，根据页码数来分开查找
                var pageIndex=queryWithPager.pageIndex;
                var pageSize=queryWithPager.pageSize;
                //获取前端传来用于查询的字段
                //判断前端传来的是不是空的

                if(!string.IsNullOrEmpty(keyWord))
                {
                   articles = articles.Include(x=>x.label).Include(x=>x.Admin).Where(x => x.ArticlesTitle.Contains(keyWord));
                }
                else
                {
                    articles = articles.Include(x=>x.label).Include(x=>x.Admin);
                }

                //查询所有的文章并且获取标签名和管理员名
                var u=articles.Skip((pageIndex-1)*pageSize).Take(pageSize).ToList();
                //查询

                var res =  new
            {
                Code = 1000,
               Data = new {Data = u,Pager =new {pageIndex,pageSize,rowsTotal=articles.Count()}},
                Msg = "获取用户列表成功^_^"
            };
            var returnString = JsonHelper.Serialize(res);
            return returnString;

            }


        [HttpGet("{id}")]
        //根据Id查询文章，就是打开具体文章了，阅读量再加上一条记录，一个是UserId一个是
        public dynamic Get(int id,RefreshTokenDTO refresh)
        {
            //查找数据库相关文章列表
            var artit = _articlesRepository.GetById(id);

            //根据前台返回的用户id来。。。这段好像可以优化
            var UserId = TokenHelper.UserValidateToken(_ToKenParmeter, refresh);

            //判断有没有这个用户的Id，有就添加阅读表，没有一样显示数据不添加阅读表
            if(!string.IsNullOrEmpty(UserId))
            {
                var createRead = new UserRead
                {
                    UserId = int.Parse(UserId),
                    ArticleId = id,
                };
                 _userReadRepository.Insert(createRead);
            }
            //更具文章值，再把评论显示出来
             var comments = _commentsRepository.table.Where(x => x.ArticleId==id).ToList();

            
            return new
            {
                Code = 1000,
                Data = artit,
                comments=comments,
                Msg = "获取指定标题成功^_^"
            };
        }

        [HttpPost]


        public dynamic Post(CreateArticles newArt)
        {
            //判断文章的各项属性是否为空，有没有标题
            if(newArt.AdminId!=0 && newArt.LabelId!=0 && newArt.ArticleContent!=null && newArt.ArticleSynopsis!= null && newArt.ArticlesTitle != null)
            {
                if(string.IsNullOrEmpty(newArt.avatar))
                {
                    newArt.avatar = "Images/Articleimage.jpg";
                }
                //都有了，就可以添加了
                var articles = new Articles
                {

                    ArticlesTitle = newArt.ArticlesTitle,
                    ArticleContent = newArt.ArticleContent,
                    ArticleSynopsis = newArt.ArticleSynopsis,
                    AdminId = newArt.AdminId,
                    LabelId = newArt.LabelId,
                    recommend = newArt.recommend,
                    ArticleImage=newArt.avatar                       
                };
                _articlesRepository.Insert(articles);

            return JsonHelper.Serialize(new
            {
                Code = 1000,
                Data = articles,
                Msg = "添加文章列表成功^_^"
            });
            }else
            {
                //没有就返回104报错
                return JsonHelper.Serialize(new
            {
                Code = 104,
                Data = "",
                Msg = "有问题"
            });
            }
        }
        
        [HttpPost, Route("{id}/Comment")]
        // 评论实现
        // 使用post请求，因为已经打开了文章，所以评论的路由是后缀名有文章的id，应该是如下这样
        public dynamic Post(int id,CreateComment createComment)
        {
          
            var insertComments = new Comments
                {
                    UserId=createComment.UserId,
                    ArticleId=id,
                    CommentContent=createComment.CommentContent                     
                };
                _commentsRepository.Insert(insertComments);

            return  new
                {
                    Code = 104,
                    Data = insertComments,
                    Msg = "添加成功"
                };
        }

       [HttpPost, Route("{id}/like")]
        //根据Id查询文章，就是打开具体文章了，阅读量再加上一条记录，一个是UserId一个是
        public dynamic PostLike(int id,RefreshTokenDTO refresh)
        {
            //转化id
            var UserId1 = TokenHelper.UserValidateToken(_ToKenParmeter, refresh);
            var userid=int.Parse(UserId1);
            //查询点赞表，查看有无用户的点赞
            var like = _userLikeRepository.table.Where(x => x.UserId ==userid && x.ArticleId == id).SingleOrDefault();
            if(like != null)
            {
                //如果有数据，那就改变启用值
                if(like.IfLike==true)
                {
                    like.IfLike=false;
                }else
                {
                    like.IfLike=true;
                }
                 _userLikeRepository.update(like);
                 return new
                {
                    Code = 104,
                    Data = like,
                    Msg = "修改成功"
                };
            }else
            {
                //如果没有数据，那就添加数据
                var liketable = new UserLike
                {
                    UserId=userid,
                    ArticleId=id,
                    IfLike=true,
                };
                _userLikeRepository.Insert(liketable);
                return new
                {
                    Code = 104,
                    Data = liketable,
                    Msg = "添加成功"
                };
            }
        }
         [HttpPut("{id}")]
        public dynamic Put(int id)
        {
            var article = _articlesRepository.GetById(id);
            
            if(article == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "要更新的用户不存在，请确认后重试=_="
                };
            }
            if(article.recommend)
            {
                  article.recommend=false;
            }
            else
            {
                  article.recommend=true;
            }
             _articlesRepository.update(article);

             return JsonHelper.Serialize(
                 new
                 {
                     Code=1000,
                     Data = article,
                     Msg = "修改成功"
                 }
             );
        }
     
            [HttpDelete("{id}")]
        public dynamic Delete(int id)
        {
            var article = _articlesRepository.GetById(id);
            if (article != null)
            {
                
                _articlesRepository.Delete(id);
                return new
                {
                    Code = 1000,
                    Msg = string.Format("删除指定id为{0}的文章成功", id)
                };
            }
            else
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "指定Id的文章不存在，请确认后重试"
                };
            }

        }
    }
}