using CMS_data.api.Entity;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using CMS_data.api.Database;
using CMS_data.api.IRepository;

namespace TwoWebApi.Backend.Api.Repository
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private CMSdataDbHerder _Db;

        public EfRepository(CMSdataDbHerder db)
        {
            _Db=db;
        }

        private DbSet<T> _Entity;

        protected DbSet<T> Entity
        {
            get
            {
                if(_Entity == null)
                {
                    _Entity = _Db.Set<T>();
                }
                return _Entity;
            }
        }

        public IQueryable<T> table
        {
            get
            {
                return Entity.AsQueryable<T>();
            }
        }

        public void Delete(object id)
        {
            var t = Entity.Find(id);

            if(t==null)
            {
                throw new ArgumentNullException(nameof(t));
            }

            _Db.Remove(t);
            _Db.SaveChanges();
        }

        public void DeleteBulk(IEnumerable<object> Ids)
        {
            var li = new List<object>();
            foreach (var item in Ids)
            {
                var tmp = (int)item;
                li.Add(tmp);
            }
            var ts = Entity.Where(x=>li.Contains(x.Id)).ToList();
            _Db.RemoveRange(ts);
            _Db.SaveChanges();
        }

        public T GetById(object id)
        {
            return Entity.Find(id);
        }

        public void Insert(T entity)
        {
            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            entity.DisplayOrder = 1;

            Entity.Add(entity);
            _Db.SaveChanges();
        }

        public async Task InsertAsync(T entity)
        {
            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            entity.DisplayOrder = 0;

            await Entity.AddAsync(entity);
            await _Db.SaveChangesAsync();
        }

        public void InsertBulk(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
            item.IsActived = true;
            item.IsDeleted = false;
            item.CreatedTime = DateTime.Now;
            item.UpdatedTime = DateTime.Now;
            item.DisplayOrder = 0;

            }

            Entity.AddRange(entities);
            _Db.SaveChanges();
        }

        public async void InsertBulkAsync(IEnumerable<T> entities)
        {
             foreach (var item in entities)
            {
            item.IsActived = true;
            item.IsDeleted = false;
            item.CreatedTime = DateTime.Now;
            item.UpdatedTime = DateTime.Now;
            item.DisplayOrder = 0;

            }

           await Entity.AddRangeAsync(entities);
            await _Db.SaveChangesAsync();
        }

        public void update(T entity)
        {
            entity.UpdatedTime=DateTime.Now;
            _Db.SaveChanges();
        }

        public void updateBulk(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                item.UpdatedTime=DateTime.Now;
            }
            Entity.UpdateRange(entities);
            _Db.SaveChanges();
        }
    }
}