using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using CMS_data.api.Entity;
using CMS_data.api.Params;

namespace  CMS_data.api.Utils
{
    public class TokenHelper
    {
        public static string GenerateToken(TokenParameter tokenParameter,string userName)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.Role, "我相信能行"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(tokenParameter.Issuer, null, claims, expires: DateTime.UtcNow.AddMinutes(tokenParameter.AccessExpiration), signingCredentials: credentials);

            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return token;

        }

        public static string GenerateToken(TokenParameter toKenParmeter, Admin user)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, "我相信能行"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(toKenParmeter.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(toKenParmeter.Issuer, null, claims, expires: DateTime.UtcNow.AddMinutes(toKenParmeter.AccessExpiration), signingCredentials: credentials);

            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return token;

        }
         /// <summary>
        /// 验证token并从中得到用户名
        /// </summary>
        /// <param name="tokenParameter">token设置</param>
        /// <param name="refresh">刷新token的模型，里面是token和refreshToken</param>
        public static string ValidateToken(TokenParameter tokenParameter,RefreshTokenDTO refresh)
        {
            var handler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal claim = handler.ValidateToken(refresh.Token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                }, out SecurityToken securityToken);

                return claim.Identity.Name;
            }
            catch
            {
                return null;
            }
        }
        //从前端获取用户Id
        public static string UserValidateToken(TokenParameter tokenParameter,RefreshTokenDTO refresh)
        {
            return refresh.UserId.ToString();
        }
         //从前端获取管理Id
        public static string AdminValidateToken(TokenParameter tokenParameter,RefreshTokenDTO refresh)
        {
            return refresh.AdminId.ToString();
        }
    }
    
}