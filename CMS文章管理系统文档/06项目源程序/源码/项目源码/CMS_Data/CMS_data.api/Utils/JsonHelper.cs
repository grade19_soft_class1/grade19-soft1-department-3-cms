using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace CMS_data.api.Utils
{
    public class JsonHelper
    {
        public static string Serialize(object obj)
        {
            var DateTimeFormats = "yyyy-MM-dd HH:mm:ss";

            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            settings.DateFormatString = DateTimeFormats;

            var timeConverter = new IsoDateTimeConverter { DateTimeFormat = DateTimeFormats };
            
            return JsonConvert.SerializeObject(obj, Formatting.Indented, settings);
        }
    }
}