namespace CMS_data.api.Utils
{
    public class QueryWithPager
    {

        public string keyWord{get;set;}

        public int pageIndex{get;set;}

        public int pageSize{get;set;}

        public int RowsTotal{get;set;}

    }
}