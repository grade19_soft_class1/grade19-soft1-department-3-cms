using CMS_data.api.Entity;

namespace CMS_data.api.Params
{
    public class CreateArticles
    {

        public string ArticlesTitle{get;set;}
        
        
        public string ArticleContent {get;set;}

        public string ArticleSynopsis{get;set;}
        
        public bool  Check {get;set;}
         public bool  recommend {get;set;}

     
        public int  LabelId {get;set;}
        public int AdminId {get;set;}

        public string AdminName {get;set;}

        public string LabelName{get;set;}

        public string avatar{get;set;}

    }
}
