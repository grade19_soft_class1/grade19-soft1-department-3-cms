using System;

namespace CMS_data.api.Params
{

    public class RefreshTokenDTO
    {
        public string Token { get; set; } 
        public string refreshToken { get; set; }

        public int UserId{get;set;}

        public int AdminId{get;set;}
    }

}
