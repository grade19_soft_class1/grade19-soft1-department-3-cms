using CMS_data.api.Entity;

namespace MS_data.api.Params
{
    public class InheritArticles:Articles
    {
        public string AdminName {get;set;}

        public string LabelName{get;set;}
    }
}