namespace CMS_data.api.Params
{
    public class CreateAdmin
    {
        public string RootName { get; set; }
        public string Pass { get; set; }

        public int PowerInfoId { get; set; }
    }

}