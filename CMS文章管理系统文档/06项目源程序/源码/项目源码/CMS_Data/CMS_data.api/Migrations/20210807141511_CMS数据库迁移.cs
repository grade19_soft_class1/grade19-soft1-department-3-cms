﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace CMS_data.api.Migrations
{
    public partial class CMS数据库迁移 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    PassWord = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    PowerInfoId = table.Column<int>(type: "integer", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    ArticleId = table.Column<int>(type: "integer", nullable: false),
                    CommentContent = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Label",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LabelName = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Label", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PowerInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PowerName = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PowerInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLike",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    ArticleId = table.Column<int>(type: "integer", nullable: false),
                    IfLike = table.Column<bool>(type: "boolean", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLike", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserRead",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    ArticleId = table.Column<int>(type: "integer", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRead", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    PassWord = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    PersonalizedName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AdminId = table.Column<int>(type: "integer", nullable: false),
                    ArticlesTitle = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    ArticleContent = table.Column<string>(type: "character varying(5000)", maxLength: 5000, nullable: true),
                    ArticleSynopsis = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    LabelId = table.Column<int>(type: "integer", nullable: false),
                    ArticleImage = table.Column<string>(type: "text", nullable: true),
                    recommend = table.Column<bool>(type: "boolean", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DisplayOrder = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_Admin_AdminId",
                        column: x => x.AdminId,
                        principalTable: "Admin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Articles_Label_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Label",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Admin",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "PassWord", "PowerInfoId", "Remarks", "UpdatedTime", "UserName" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9619), 1, true, false, "113", 1, null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9625), "king" },
                    { 2, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9661), 1, true, false, "113", 2, null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9662), "Queen" },
                    { 3, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9664), 1, true, false, "113", 3, null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9665), "Knight" },
                    { 4, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9667), 1, true, false, "113", 4, null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(9668), "pawn" }
                });

            migrationBuilder.InsertData(
                table: "Label",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "LabelName", "Remarks", "UpdatedTime" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 22, 15, 11, 91, DateTimeKind.Local).AddTicks(2391), 1, true, false, "新闻热点", null, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(5831) },
                    { 2, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(6383), 1, true, false, "公司动态", null, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(6394) },
                    { 3, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(6423), 1, true, false, "大神风采", null, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(6425) },
                    { 4, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(6427), 1, true, false, "了解我们", null, new DateTime(2021, 8, 7, 22, 15, 11, 92, DateTimeKind.Local).AddTicks(6428) }
                });

            migrationBuilder.InsertData(
                table: "PowerInfo",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "PowerName", "Remarks", "UpdatedTime" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6843), 1, true, false, "King", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6850) },
                    { 2, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6874), 1, true, false, "Queen", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6876) },
                    { 3, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6878), 1, true, false, "Knight", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6878) },
                    { 4, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6880), 1, true, false, "Pawn", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(6881) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "PassWord", "PersonalizedName", "Remarks", "UpdatedTime", "UserName" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8218), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8224), "admin1" },
                    { 2, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8261), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8262), "admin2" },
                    { 3, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8264), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8265), "admin3" },
                    { 4, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8267), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 22, 15, 11, 93, DateTimeKind.Local).AddTicks(8268), "admin4" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articles_AdminId",
                table: "Articles",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_LabelId",
                table: "Articles",
                column: "LabelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "PowerInfo");

            migrationBuilder.DropTable(
                name: "UserLike");

            migrationBuilder.DropTable(
                name: "UserRead");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "Label");
        }
    }
}
