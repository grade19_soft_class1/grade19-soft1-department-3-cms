﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CMS_data.api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PassWord = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PowerInfoId = table.Column<int>(type: "int", nullable: false),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ArticleId = table.Column<int>(type: "int", nullable: false),
                    CommentContent = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Label",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LabelName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Label", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PowerInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PowerName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PowerInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserLike",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ArticleId = table.Column<int>(type: "int", nullable: false),
                    IfLike = table.Column<bool>(type: "bit", nullable: false),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLike", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserRead",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ArticleId = table.Column<int>(type: "int", nullable: false),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRead", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PassWord = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PersonalizedName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AdminId = table.Column<int>(type: "int", nullable: false),
                    ArticlesTitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ArticleContent = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: true),
                    ArticleSynopsis = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LabelId = table.Column<int>(type: "int", nullable: false),
                    ArticleImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    recommend = table.Column<bool>(type: "bit", nullable: false),
                    IsActived = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_Admin_AdminId",
                        column: x => x.AdminId,
                        principalTable: "Admin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Articles_Label_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Label",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Admin",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "PassWord", "PowerInfoId", "Remarks", "UpdatedTime", "UserName" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9693), 1, true, false, "113", 1, null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9698), "king" },
                    { 2, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9730), 1, true, false, "113", 2, null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9731), "Queen" },
                    { 3, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9733), 1, true, false, "113", 3, null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9734), "Knight" },
                    { 4, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9736), 1, true, false, "113", 4, null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(9737), "pawn" }
                });

            migrationBuilder.InsertData(
                table: "Label",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "LabelName", "Remarks", "UpdatedTime" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 9, 34, 55, 901, DateTimeKind.Local).AddTicks(9128), 1, true, false, "新闻热点", null, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(7885) },
                    { 2, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(8473), 1, true, false, "公司动态", null, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(8484) },
                    { 3, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(8501), 1, true, false, "大神风采", null, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(8502) },
                    { 4, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(8504), 1, true, false, "了解我们", null, new DateTime(2021, 8, 7, 9, 34, 55, 902, DateTimeKind.Local).AddTicks(8505) }
                });

            migrationBuilder.InsertData(
                table: "PowerInfo",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "PowerName", "Remarks", "UpdatedTime" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7483), 1, true, false, "King", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7490) },
                    { 2, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7513), 1, true, false, "Queen", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7514) },
                    { 3, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7516), 1, true, false, "Knight", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7516) },
                    { 4, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7518), 1, true, false, "Pawn", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(7519) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedTime", "DisplayOrder", "IsActived", "IsDeleted", "PassWord", "PersonalizedName", "Remarks", "UpdatedTime", "UserName" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8580), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8584), "admin1" },
                    { 2, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8622), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8623), "admin2" },
                    { 3, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8625), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8626), "admin3" },
                    { 4, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8628), 1, true, false, "113", "轻轻的我来了", null, new DateTime(2021, 8, 7, 9, 34, 55, 903, DateTimeKind.Local).AddTicks(8629), "admin4" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articles_AdminId",
                table: "Articles",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_LabelId",
                table: "Articles",
                column: "LabelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "PowerInfo");

            migrationBuilder.DropTable(
                name: "UserLike");

            migrationBuilder.DropTable(
                name: "UserRead");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "Label");
        }
    }
}
