import Index from '../views/Index.vue'
import Login from '../views/Login.vue'
import UserHome from '../views/UserHome.vue'
import Register from '../views/Register.vue'
import AdminHome from '../AdministratorModule/AdminHome.vue'
import ArticleRecommend from '../views/ArticleRecommend.vue'
import NewsHotspot from '../views/NewsHotspot.vue'
import BigBrother from '../views/BigBrother.vue'
import UnderstandUs from '../views/UnderstandUs.vue'
import CompanyDynamics from '../views/CompanyDynamics.vue'
import UserRemove from '../views/UserRemove.vue'
import Userinfo from '../views/Userinfo.vue'
import WALranking from '../views/WALranking.vue'
import WARranking from '../views/WARranking.vue'
import WArecommendation from '../views/WArecommendation.vue'
import Userpwd from '../views/Userpwd.vue'


// 遍历路由要设立隐藏属性，用户界面路由不能遍历到管理员界面
// 模式为：
//  meta: {
//   title: '根目录',
//   icon: 'el-icon-phone-outline',
//   hidden: true  true为隐藏路由目录
// }




let routes = [
  {
    path: '/',
    name: 'Index',
    meta: {
      title: '根目录',
      icon: 'el-icon-phone-outline',
     
      hidden: true,
    },
    component: Index,
 
  },

  // 用户模块路由
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '用户登陆',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: Login
  },
  {
    path: '/UserHome',
    name: 'UserHome',
    meta: {
      title: '用户首页',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: UserHome
  }
  , {
    path: '/Register',
    name: 'Register',
    meta: {
      title: '用户注册',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: Register
  },
  {
    path: '/ArticleRecommend',
    name: 'ArticleRecommend',
    meta: {
      title: '文章推荐',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: ArticleRecommend
  },
  {
    path: '/NewsHotspot',
    name: 'NewsHotspot',
    meta: {
      title: '新闻热点',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: NewsHotspot
  },
  {
    path: '/BigBrother',
    name: 'BigBrother',
    meta: {
      title: '大神风采',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: BigBrother
  },
  {
    path: '/UnderstandUs',
    name: 'UnderstandUs',
    meta: {
      title: '关于我们',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: UnderstandUs
  }
  ,
  {
    path: '/CompanyDynamics',
    name: 'CompanyDynamics',
    meta: {
      title: '公司动态',
      icon: 'el-icon-phone-outline',
      hidden: true,
    },
    component: CompanyDynamics
  }, {
    path: '/Userpwd',
    name: 'Userpwd',
    meta: {
      title: '密码重置',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: Userpwd
  }, {
    path: '/UserRemove',
    name: 'UserRemove',
    meta: {
      title: '用户信息修改',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: UserRemove
  }
  , {
    path: '/Userinfo',
    name: 'Userinfo',
    meta: {
      title: '用户信息',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: Userinfo
  }
  , {
    path: '/WALranking',
    name: 'WALranking',
    meta: {
      title: '周评论最多',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: WALranking
  }
  , {
    path: '/WARranking',
    name: 'WARranking',
    meta: {
      title: '周排行',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: WARranking
  }
  , {
    path: '/WArecommendation',
    name: 'WArecommendation',
    meta: {
      title: '周推荐',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: WArecommendation
  },









  // 管理员模块路由
 {
    path: '/Adminhome',
    name: 'Adminhome',
    component: AdminHome,
    meta: {
      title: '管理员首页',
      icon: 'el-icon-phone-outline',
      requireAuth:true,
      hidden:true

    },
    children: [
      {
        path: 'echarts',
        meta: {
          title: '管理员首页',
          icon: 'el-icon-coin',
          // hidden:true
        },
        component: () => import('../AdministratorModule/Echrts'),
      },
    ],
  },


  {
    path: '/Adminlogin',
    name: 'Adminlogin',
    meta: {
      title: '管理员登陆',
      icon: 'el-icon-phone-outline',
      hidden: true
    },
    component: () => import('../AdministratorModule/AdminLogin')

  },

  {
    path: '/Admininfo',
    component: AdminHome,
    name: 'Admininfo',
    meta: {
      title: '管理员设置',
      icon: 'el-icon-goods',

    },
    children: [
      {
        path: 'adminadd',
        name: 'adminadd',
        meta: {
          title: '添加管理员',
          icon: 'el-icon-warning-outline',

        },
        component: () => import('../AdministratorModule/AdminAdd'),
      },
      {
        path: 'Permissions',
        name: 'Permissions',
        meta: {
          title: '禁用用户',
          icon: 'el-icon-edit-outline',
        },
        component: () => import('../AdministratorModule/Permissions'),
      }
    ],
  },

  {
    path: '/Articleinfo',
    component: AdminHome,
    name: 'Articleinfo',
    meta: {
      title: '文章管理设置',
      icon: 'el-icon-goods',

    },
    children: [
      {
        path: 'ArticleAudit',
        name: 'ArticleAudit',
        meta: {
          title: '文章发布',
          icon: 'el-icon-warning-outline',
        },
        component: () => import('../AdministratorModule/ArticleAudit'),
      },

      {
        path: 'ArticleConentInfo',
        name: 'ArticleConentInfo',
        meta: {
          title: '文章管理',
          icon: 'el-icon-edit-outline',
        },
        component: () => import('../AdministratorModule/ArticleRecommendInfo'),
      },
      {
        path: 'BottomInfo',
        name: 'BottomInfo',
        meta: {
          title: '底部模块',
          icon: 'el-icon-edit-outline',
          hidden:true
        },
        component: () => import('../AdministratorModule/BottomInfo'),
      }

    ],
  },


]


export default routes