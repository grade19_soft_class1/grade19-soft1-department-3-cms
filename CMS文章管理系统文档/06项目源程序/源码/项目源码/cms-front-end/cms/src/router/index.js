import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'




Vue.use(VueRouter)



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to,from,next)=>{
    
 let flag= localStorage.getItem('adminId')

  if(to.meta.requireAuth == true){ // 需要登录权限进入的路由
      if(!flag){                   // 获取不到登录信息
          next({
              path: '/adminlogin'
          })
      }else{                       // 获取到登录信息，进行下一步
          return next();
      }
  }else{                           // 不需要登录权限的路由直接进行下一步
      return next();
  }
})


export default router
