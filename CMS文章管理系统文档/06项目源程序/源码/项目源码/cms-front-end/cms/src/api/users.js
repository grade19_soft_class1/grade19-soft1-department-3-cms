import request from '../utils/request'


//根据指定id获取指定用户
export function getById(id){
    return request.get(`/users/${id}`)
}

//获取用户列表
export function getUser(params){
    return request.get('/users',{params:params})
}

//添加用户
export function addUser(data){
    return request.post('/users',data)
}

//修改用户
export function modUser(id,data){
    console.log(data);
    return request.put(`/users/${id}`,data)
}

//禁用用户
export function putUserIsActived(id) {
    return request.put(`/users/${id}/IsActived`)
}

//逻辑删除用户用户
export function putUserIsDelete(id) {
    return request.delete(`/users/${id}`)
}

//用户登录，获取token
export function login(data){
    return request.post('/users/token',data)
}
//管理员登入获取token
export function AdminLogin(data)
{
    return request.post('/admin/token',data)
}
