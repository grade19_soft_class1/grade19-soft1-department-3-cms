import request from '../utils/request'

export function getList(params){
    return request.get('/Articles',{params:params})
}
export function putArticle(id){
    return request.put(`/Articles/${id}`)
}
export function addArticle(data)
{
    console.log(data);
    return request.post("/Articles",data)
}

export function delArticle(id){
    return request.delete(`/Articles/${id}`)
}


export function getArticleData()
{
    return request.get("/Index")
}



export function addComment(id,data)
{
    console.log(data);
    return request.post(`/Articles/${id}/Comment`,data)
}