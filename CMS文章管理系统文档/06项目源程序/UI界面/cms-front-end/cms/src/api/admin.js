import request from '../utils/request'

//管理员登入获取token
export function AdminLogin(data)
{  
    return request.post('/admin/token',data)
    
}

//获取管理员列表
export function getAdminList(params){
    return request.get('/admin',{params:params})
}

//添加管理员
export function addAdminUser(data){
  
    return request.post('/admin',data)
}
