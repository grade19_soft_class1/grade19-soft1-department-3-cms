// 定义一些常量
const TokenKey='accessToken'
const RefreshTokenKey='refreshToken'

// 获取token
export function getToken(){
  return localStorage.getItem(TokenKey) || ''
}

// 获取refreshToken
export function getRefreshToken(){
  return localStorage.getItem(RefreshTokenKey) || ''
}

// 保存token和refreshToken
export function setToken(token,refreshToken){
  localStorage.setItem(TokenKey,token)
  localStorage.setItem(RefreshTokenKey,refreshToken)
}

// 清除token和refreshToken
export function clearAdminToken(){
  localStorage.removeItem('adminId')
  localStorage.removeItem(TokenKey)
  localStorage.removeItem(RefreshTokenKey)
  localStorage.removeItem('AdminName')
}
export function clearUserToken(){
  localStorage.removeItem('UserId')
  localStorage.removeItem(TokenKey)
  localStorage.removeItem(RefreshTokenKey)
  localStorage.removeItem('UserName')
}

export function AdminName(){
 
return localStorage.getItem('AdminName')

}

export function UserName(){
return  localStorage.getItem('UserName')
}

export function getUserId()
{
    return localStorage.getItem('UserId')
}
export function getAdminId()
{
    return localStorage.getItem('adminId')
}


// 判断是否为登录状态
export function isLogin(){
  let token=getToken()
  if(token){
    return true
  }else{
    return false
  }
}
