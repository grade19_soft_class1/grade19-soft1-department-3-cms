# put请求的坏处

总结，出错了都不知道怎么出错的，所以听胡哥的，少用put请求，post请求天下第一，不接受反驳

# 优化点赞的put请求

我之前是这样使用put请求的


```
[HttpPut("{id}")]

 public dynamic put(int id,RefreshTokenDTO refresh)
 {

 }

```

如果这样写，之后再有put请求就接受会出现错误，提示有多个路由，不管带后面有没有加后缀名

所以优化完是这样

```
[HttpPost, Route("{id}/like")]

public dynamic PostLike(int id,RefreshTokenDTO refresh)
{

}

```
